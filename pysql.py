import MySQLdb
import bottle_mysql

def get_id(db, table, field, max_min):
  query = "SELECT %s(%s) FROM %s;" % (max_min, field, table)
  db.execute(query)
  id = db.fetchone()
  print 'getting id'
  print id
  if id["%s(%s)" % (max_min, field)] == None:
    return 0
  else:
    return id["%s(%s)" % (max_min, field)]

def get_votes(db, set_id, spec_id):
  db.execute("SELECT votes FROM Contents WHERE Obs_id=%d AND Species_id=%d" % (set_id, spec_id))
  votes = db.fetchone()
  print votes
  if votes == None:
    return 0
  else:
    return votes['votes']

def check_contents(db, set_id, spec_id):
  db.execute("SELECT * FROM Contents WHERE Obs_id = %d and Species_id = %d;" % (set_id, spec_id))
  if db.fetchone():
    print 'Found content'
    return True
  else:
    print 'No content found'
    return False
