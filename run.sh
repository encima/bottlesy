uwsgi --socket :3051 --wsgi-file ./web.py --daemonize /usr/share/nginx/html/bottlesy/logs/bottlesy.log --callable=app --catch-exceptions

echo "`pgrep uwsgi`"
