from bottle import route, run, debug, template, request, validate, static_file, error, get, redirect, hook, HTTPError
# only needed when you run Bottle on mod_wsgi
from bottle import default_app
import bottle_mysql
import os
import json
import pysql
from beaker.middleware import SessionMiddleware

session_opts = {
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.auto': True
}

@hook('before_request')
def setup_request():
  request.session = request.environ.get('beaker.session')
  if 'classif_count' not in request.session:
  	request.session['classif_count'] = 0

@route('/')
def index():
	redirect('/10103')

@route('/explore/')
def data_explorer(db):
	location_query = "SELECT * FROM Locations"
	db.execute(location_query)
	points = db.fetchall()
	return template('views/explorer', points=points)

@route('/<id>')
def show_set(id, db):
	# if 'classif_count' in request.session and request.session['classif_count'] >= 20:
	# 	request.session['classif_count'] = 0
	# 	return template('views/cflower')
	print id
	query_set = "SELECT o.Obs_ID, group_concat(i.path ORDER BY i.path ASC SEPARATOR ';') AS path, s.Name, c.Votes FROM Observations o left join Contents c on o.Obs_ID = c.Obs_ID left join Species s on c.Species_ID = s.Species_ID left join ImageSets i on i.set_id = o.Obs_ID WHERE o.Obs_ID = " + id + " GROUP BY s.Name;"
	db.execute(query_set)
	rows = db.fetchall()
	if db.rowcount > 0:
		paths = None
		obs_id = None
		votes = {}
		for row in rows:
			obs_id = row['Obs_ID']
			paths = row['path'].split(';')
			votes[row['Name']] = row['Votes']
		db.execute('SELECT Name, Image from Species WHERE Review=0;')
		species = db.fetchall()
		return template('views/index', paths = paths, id=obs_id, votes = votes, count = request.session['classif_count'], species =species)
	else:
		redirect('/10103')

@route('/vote', method='POST')
def submit_vote(db):
	request.session['classif_count'] += 1
	print '***YOUVE GOT POST!!!****'
	set_id = int(request.forms.get('id'))
	contents = request.forms.get('contents')
	url = request.forms.get('url')
	species_query = """SELECT species_id, name FROM Species WHERE LOWER(name) = LOWER("%s");""" % contents
	db.execute(species_query)
	row = db.fetchone()
	spec_id = None
	# Check is species exists
	if(row):
		print row['name']
		spec_id = row['species_id']
	else:
		print 'Species not found; creating...'
		spec_id = pysql.get_id(db, "Species", "species_id", "MAX") + 1
		species_insert = "INSERT INTO Species VALUES (%d, '%s', '%s', 1, 'cflower1');" % (spec_id, contents, url)
		db.execute(species_insert)
	votes = pysql.get_votes(db, set_id, spec_id) + 1
	print votes
	# Check if the image has been voted on before, add the new row if not
	if(pysql.check_contents(db, set_id, spec_id)):
		db.execute("UPDATE Contents SET votes = %d WHERE Obs_ID = %d AND Species_ID = %d;" % (votes, set_id, spec_id))
	else:
		cont_id = pysql.get_id(db, "Contents", "Contents_ID", "MAX") + 1
		print 'Adding to Contents'
		if cont_id ==  None:
			cont_id = 0
		insert = "INSERT INTO Contents VALUES (%d, %d, %d, %d);" % (cont_id, set_id, spec_id, votes)
		print insert
		db.execute(insert)
		print 'Added to Contents'
	result = {'name': contents, 'votes': votes}
	print result
	return result

if __name__ == '__main__':
	#starts wsgi server iif run with bottle
	print 'Running without nginx, using own wsgi server'
	run(reloader=True, port=3031, host='0.0.0.0')
	os.chdir(os.path.dirname(__file__))
	application = default_app()
	plugin = bottle_mysql.Plugin(dbuser='sitesy', dbpass='44Lumcit', dbname='sitesy', dbhost='sitesy.ci1bmxc9puk4.eu-west-1.rds.amazonaws.com')
	print 'Running Bottlesy'
	application.install(plugin)
	application = SessionMiddleware(application, session_opts)
else:
	#called by uwsgi,
	os.chdir(os.path.dirname(__file__))
	application = default_app()
	plugin = bottle_mysql.Plugin(dbuser='sitesy', dbpass='44Lumcit', dbname='sitesy', dbhost='sitesy.ci1bmxc9puk4.eu-west-1.rds.amazonaws.com')
	print 'Running Bottlesy'
	application.install(plugin)
	application = SessionMiddleware(application, session_opts)
