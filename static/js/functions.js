// Browser detection for when you get desparate. A measure of last resort
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }

var set = window.location.pathname.replace( /^\D+/g, '');
var mouse_is_inside = false;

/* trigger when page is ready */
$(document).ready(function (){
    var $thumbs = $('#thumbs li');
	  $thumbs.first().addClass('active');
    $thumbs.first().find('img').css('border', '5px');
  
    $('#instruct').hide();
    $('#overlay').hide();
    $('#submit').attr('disabled', true);

    //check for mouse clicks outside of the popup
    $('#overlay').hover(function(){ 
        mouse_is_inside = true; 
    }, function(){ 
        mouse_is_inside = false; 
    });

    // Listen for keypresses and remove numbers
    $('.alphaonly').bind('keyup blur',function(){ 
    	$(this).val( $(this).val().replace(/[^a-zA-Z]/g,'') ); }
    );
    
    $("body").mouseup(function(){ 
        if(! mouse_is_inside) {
          $('#overlay').hide();
        }
    });

    $(window).keyup(function(e){
       var $current = $thumbs.filter('.active');
       var $new; 
        if (e.keyCode == 40 && $("#contents").is(':focus') == false) {
          // Down
          $new = $current.next();
          console.log($new.find('img'));
          if(!$new.length) $new = $thumbs.first();
          $('#main-img').attr('src', $new.find('img').attr('src'));
        } else if (e.keyCode == 38 && $("#contents").is(':focus') == false) {
          // Up
          $new = $current.prev();
          if(!$new.length) $new = $thumbs.last();
          $('#main-img').attr('src', $new.find('img').attr('src'));
        } else if (e.keyCode == 37 && $("#contents").is(':focus') == false) {
            //Left
            if(parseInt(set)>0)
                window.location.pathname = '/' +  --set;
        } else if (e.keyCode == 39 && $("#contents").is(':focus') == false) {
            //Right
            window.location.pathname = '/' + ++set;
        }
        if($new) {
          $current.removeClass('active');
          $new.addClass('active');
        }
    });

    $("#set-text").keyup(function(event){
        if(event.keyCode == 13 && $("#set-text").val() != ""){
            console.log($("#set-text"));
            window.location.pathname = '/' + $("#set-text").val();
        }
    });

    $('.small-img').click(function() {
      saveClassification($(this).attr('alt'));
    });

    $("#contents").keyup(function(e){
	   if(e.keyCode == 13) return false;
    });

    $("#rand_btn").click(function(e) {
        window.location.pathname = '/' + (Math.floor(Math.random() * (10683-10103+1)) + 10103);
    });



    $('#instr-tab').click(function() {
      if($('#instruct').is(':visible')) {
        $('#instruct').slideUp("slow");  
        $('#instr-tab').css('color', 'white');
      } else {
        $('#instruct').slideDown("slow");  
        $('#instr-tab').css('color', '#B2382D');
      }
      // $('#instruct').toggle(function() {
      //   $('#instruct').slideDown('fast');
      //   $('#instr-tab').css('color', 'white');
      // }, function() {
      //   $('#instruct').slideDown('fast');
      //   $('#instr-tab').css('color', '#B2382D');
      // });
    });

    $('#submit').click(function() {
        species = $('#contents').val();
        if(confirm('Are you sure that ' + species + ' is in this image (and not shown in the samples above? Please do not add it if not!') && species != '') {
            saveClassification(species);
          }
    });

    $('.thumb').click(function() {
      var $current = $thumbs.filter('.active');
      console.log($(this).attr('src'));
      $('#main-img').attr('src', $(this).attr('src'));
      $current.removeClass('active');
      $(this).addClass('active');
    });

    $('#contents').keyup(function(){
      if ($(this).val() == '') { //Check to see if there is any text entered
           //If there is no text within the input ten disable the button
           $('#submit').attr('disabled', true);
      } else if(!$("#empty").is(':checked')) {
           //If there is text in the input, then enable the button
           $('#submit').attr('disabled', false);
      }
    });
});

function saveClassification(species) {
  console.log(species);
  console.log($('#thumbs li:nth-child(2)').find('img').attr('src'));
  $.ajax({
      type : 'POST',
      url : 'vote',
      dataType : 'json',
      data : {
        id : set,
        contents: species,
        url: $('#thumbs li:nth-child(2)').find('img').attr('src')
      },
      success : function(data){
        console.log(data);
        console.log(data['name']);
        $('form :button[name=submit]').prop('disabled', true);
        updateTable(data);
        drawToast('<h1>Thanks for your vote!</h1>');
        window.location.pathname = '/' + (Math.floor(Math.random() * (10683-10103+1)) + 10103);
      }, error : function(XMLHttpRequest, textStatus, errorThrown){
        console.log(XMLHttpRequest);
        console.log(textStatus + ": " + errorThrown);
      }
    });
}

function updateTable(data) {
  var species = "." + data['name'] + "_votes";
  var update = data['name'] + ": " + data['votes'];
  console.log(species);
  if($(species)[0]) {
    console.log('Updating table');
    $(species).text(data['votes']);
  } else {
    console.log('adding table row');
    var tableContent = $('#classif-table').html();
   $('#classif-table').append("<tr><td class='classif'>" + data['name'] + "</td><td class=\"" + data['name'] + "_votes\">" + data['votes'] + "</td></tr>");
  }
}

function drawToast(text) {
  $('#overlay').html(text);
  console.log($('#overlay').html());
  $('#overlay').show(function() {
    setTimeout(function() {
      $("#overlay").hide('blind', {}, 500);
    }, 5000);
  });
}

