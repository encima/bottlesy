<!doctype html>

<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head id="www-sitename-com" data-template-set="html5-reset">

  <meta charset="utf-8">
  
  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>Sitesy</title>
  
  <meta name="title" content="Sitesy" />
  <meta name="description" content="Coletive Image Classification Site" />
  <meta name="author" content="Chris Gwilliams" />
  <!-- Google will often use this as its description of your page/site. Make it good. -->
  
  <meta name="google-site-verification" content="" />
  <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
  
  <meta name="author" content="Chris Gwilliams" />
  <meta name="Copyright" content="Copyright Your Name Here 2013. All Rights Reserved." />

     <script type="text/javascript">
      
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38400334-2']);
        _gaq.push(['_trackPageview']);
                      
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
  
  <!-- concatenate and minify for production -->
  <link rel="stylesheet" href="static/css/style.css" />
  <!-- Google Font: Economica -->
  <link href='http://fonts.googleapis.com/css?family=Economica:700,700italic' rel='stylesheet' type='text/css'>
  
  <!-- This is an un-minified, complete version of Modernizr. 
     Before you move to production, you should generate a custom build that only has the detects you need. -->
  <script src="static/js/modernizr-2.6.2.dev.js"></script>

</head>
<body>
<div id="wrap">
  <div id="header">
    <h1 style="display:inline;">Sitesy</h1>
    <div style="float:right;">
        Go To Set:<input type="text" id="set-text" name="set-id">
          <button class="button" id="rand_btn">Random Set</button>
    </div>
  </div>
  <div id="nav">
      <ul id="nav-list">
        <!-- <li><h2 id="classif-tab">Classify</h2></li> -->
        <li><h2 id="instr-tab">Instructions</h2></li>
      </ul>
      <!-- <h2 id="instructions">Instructions</h2> -->
  </div>

  <div id="content">
      <div id="instruct">
          <h1>Welcome to Sitesy!</h1>
          </hr>
            Thank you for taking the time to tag some animals! <br/>
            Firstly, please feel free to have a click around (use the keyboard, too)! <br/>
            Tagging images is simple:<br/>
            1. Look at the 3 images (press up/down to cycle through; or use the mouse).<br/>
            2. Fill in the animal in the box (or tick empty, if it is empty) <br/>
            3. If the table already shows the animal you think it is, click that row to cast your vote. Easy, eh?<br/>
            4. Click classify and start all over again. <br/>
            5. If you are from CrowdFlower, please do 20 and then head back to crowdflower with the code you were given! <br/>

            <h2>FAQ</h4>
            <h2>What if the pics don't load?</h2>
            Please just click 'Random Set' and try again, we have issues with certain images being hosted
            <h2>What if I do not know what is in the image?</h2>
            Again, please just click random and try again.
            <h2>How specific should I be?</h2>
            Some of you may be biologists and have some knowledge, please be as general as possible. I.e. If you see a monkey and know the species, please just put monkey down. Same goes for wild boars, leopards and bears; oh my!

            <h1>Problems? Email <a href="mailto:encima@gmail.com">me</a>! <h1>
    </div>
    <div id="main">
      <h1>Thanks!</h1>
      <h2>Your code is: 2013c0mpleted0145</h2>
      <h3>Questions? Feedback? <a href="mailto:scm7cg@cs.cf.ac.uk">Email Me!</a></h3>
    </div>

  </div>
  <div id="footer">
    <p>These images are copyright of Danau Girang Field Centre and must not be reproduced. Please contact <a href="mailto:encima@gmail.com">Chris Gwilliams</a> for more info. </p>
  </div>
  <div id="overlay">
    
  </div>
</div>

<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='static/js/jquery-1.9.1.min.js'>\x3C/script>")</script>

<!-- this is where we put our custom functions -->
<!-- don't forget to concatenate and minify if needed -->
<script src="static/js/functions.js"></script>

  
</body>
</html>
