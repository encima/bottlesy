from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
import json
import urllib
import sqlite3
import re
import MySQLdb as mdb
import pysql
# from flask import current_app as app

# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)
app.debig = True

def connect_db():
    g.conn = mdb.connect(user="sitesy",passwd="44Lumcit",db="sitesy",host="sitesy.ci1bmxc9puk4.eu-west-1.rds.amazonaws.com", charset="utf8")
    cursor = g.conn.cursor(mdb.cursors.DictCursor)
    return cursor
    # return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    g.db.close()
    g.conn.close()

@app.route('/<id>')
def show_set(id):
    print id
    query_set = "SELECT o.Obs_ID, group_concat(i.path ORDER BY i.path ASC SEPARATOR ';') AS path, s.Name, c.Votes FROM Observations o left join Contents c on o.Obs_ID = c.Obs_ID left join Species s on c.Species_ID = s.Species_ID left join ImageSets i on i.set_id = o.Obs_ID WHERE o.Obs_ID = " + id + " GROUP BY s.Name;"
    g.db.execute(query_set)
    rows = db.fetchall()
    if db.rowcount > 0:
        paths = None
        obs_id = None
        votes = {}
        for row in rows:
            obs_id = row['Obs_ID']
            paths = row['path'].split(';')
            votes[row['Name']] = row['Votes']
        g.db.execute('SELECT Name, Image from Species WHERE Review=0;')
        species = db.fetchall()
        return render_template('index.html', paths = paths, id=obs_id, votes = votes, count = request.session['classif_count'], species =species)
    else:
        redirect('/10103')

@app.route('/vote', methods=['POST'])
def submit_vote():
    set_id = int(request.form['id'])
    contents = request.form['contents']
    url = request.form['url']
    species_query = """SELECT species_id, name FROM Species WHERE LOWER(name) = LOWER("%s");""" % contents
    g.db.execute(species_query)
    row = g.db.fetchone()
    spec_id = None
    # Check is species exists
    if(row):
        print row['name']
        spec_id = row['species_id']
    else:
        print 'Species not found; creating...'
        spec_id = pysql.get_id(g.db, "Species", "species_id", "MAX") + 1
        species_insert = "INSERT INTO Species VALUES (%d, '%s', '%s', 1, 'cflower1');" % (spec_id, contents, url)
        db.execute(species_insert)
    votes = pysql.get_votes(g.db, set_id, spec_id) + 1
    print votes
    # Check if the image has been voted on before, add the new row if not
    if(pysql.check_contents(g.db, set_id, spec_id)):
        g.db.execute("UPDATE Contents SET votes = %d WHERE Obs_ID = %d AND Species_ID = %d;" % (votes, set_id, spec_id))
    else:
        cont_id = pysql.get_id(g.db, "Contents", "Contents_ID", "MAX") + 1
        print 'Adding to Contents'
        if cont_id ==  None:
            cont_id = 0
        insert = "INSERT INTO Contents VALUES (%d, %d, %d, %d);" % (cont_id, set_id, spec_id, votes)
        print insert
        g.db.execute(insert)
        print 'Added to Contents'
    result = {'name': contents, 'votes': votes}
    print result
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0')
